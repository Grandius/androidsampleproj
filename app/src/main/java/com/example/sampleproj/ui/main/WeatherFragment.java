package com.example.sampleproj.ui.main;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.*;
/*import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;*/
import com.android.volley.toolbox.*;
/*import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;*/
import com.example.sampleproj.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A placeholder fragment containing a simple view.
 */
public class WeatherFragment extends PlaceholderFragment {

    private RequestQueue imagesQueue;
    private RecyclerView recyclerView;
    private WeatherAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setContentView(R.layout.my_activity);

    }


    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_weather, container, false);
        //create Recycler
        recyclerView = root.findViewById(R.id.RecyclerScroll);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        //String[] SampleDataset = {"lol", "kek", "lorem ipsum", "Once you have added a RecyclerView widget", "to your layout, obtain a handle to the object", "connect it to a layout manager, and attach an adapter for the data"};
        mAdapter = new WeatherAdapter();
        recyclerView.setAdapter(mAdapter);
        return root;

    }

    @Override
    public void onResume() {
        super.onResume();
        RequestWeatherFeed();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (this.imagesQueue != null) this.imagesQueue.stop();//cancel images loading
    }


    public void RequestWeatherFeed() {

        final TextView textView2 = this.getView().findViewById(R.id.section_label2);

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getContext());
        //String url = "https://api.openweathermap.org/data/2.5/weather?q=Kyiv&units=metric&APPID=" + getString(R.string.WeatherAPI_apiKey);
        String url = "https://api.openweathermap.org/data/2.5/forecast?q=Kyiv&units=metric&APPID=" + getString(R.string.WeatherAPI_apiKey);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        //final int articlesDisplayed = RefreshList(response);
                        //mAdapter.notifyDataSetChanged();

                        mAdapter.setDataset(response);

                        //textView2.setText("Response: " + response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        textView2.setText("That didn't work!" + " " + error.getMessage());

                    }
                });
        // Add the request to the RequestQueue.
        queue.add(jsonObjectRequest);

    }

    /*public int RefreshList(JSONObject json) {
        //Reset view and download queue
        LinearLayout listView = this.getView().findViewById(R.id.scrollViewLayout);
        listView.removeAllViewsInLayout();
        this.imagesQueue = Volley.newRequestQueue(getContext());

        if (json == null || !json.has("list")) {
            return 0;
        }

        //Parse articles
        JSONArray forecast;
        try {
            forecast = json.getJSONArray("list");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }

        //final int maxCount = 25;
        final int count = forecast.length();


        for (int i = 0; i < count; i++) {
            try {
                JSONObject jsonObject = forecast.getJSONObject(i);
                JSONObject mainObject = jsonObject.getJSONObject("main");
                JSONObject weatherObject = jsonObject.getJSONArray("weather").getJSONObject(0);

                View newItem = LayoutInflater.from(this.getContext()).inflate(R.layout.news_list_template, null);

                final TextView textView = newItem.findViewById(R.id.textViewNewsTemplate);
                final ImageView imageView = newItem.findViewById(R.id.imageViewNewsTemplate);


                textView.setText(Long.toString(Math.round(mainObject.getDouble("temp")))+" C "+ jsonObject.getString("dt_txt"));
                listView.addView(newItem);

                //Request image
                String imageUrl = "https://openweathermap.org/img/wn/"+weatherObject.getString("icon")+"@2x.png";
                ImageRequest imageRequest = new ImageRequest(imageUrl,
                        new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap bitmap) {

                                try {
                                    imageView.setImageBitmap(bitmap);
                                }
                                catch(Exception ex)
                                {
                                    ex.printStackTrace();
                                }

                            }
                        }, 0, 0, null,
                        new Response.ErrorListener() {
                            public void onErrorResponse(VolleyError error) {
                                //imageView.setImageResource(R.drawable.image_load_error);
                            }
                        });

                this.imagesQueue.add( imageRequest );
            } catch (JSONException e) {
                e.printStackTrace();

            }
        }
        return count;
    }*/
}