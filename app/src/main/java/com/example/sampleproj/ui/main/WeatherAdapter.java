package com.example.sampleproj.ui.main;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.example.sampleproj.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.MyViewHolder> {

    private JSONObject mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View view;
        public TextView textView;
        public ImageView imageView;
        public RequestQueue imagesQueue;

        public MyViewHolder(View v) {
            super(v);
            view = v;
            textView = view.findViewById(R.id.textViewNewsTemplate);
            imageView = view.findViewById(R.id.imageViewNewsTemplate);
            imagesQueue = Volley.newRequestQueue(view.getContext());
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)

    public WeatherAdapter() {
    }

    public void setDataset(JSONObject jsonObject) {

        mDataset = jsonObject;
        this.notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public WeatherAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        // create a new view

        View newItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_template, null);

        MyViewHolder vh = new MyViewHolder(newItem);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element


        if (mDataset != null) {

            JSONArray forecast = mDataset.optJSONArray("list");
            if (forecast != null) {

                final int count = forecast.length();
                try {
                    JSONObject jsonObject = forecast.getJSONObject(position);
                    JSONObject mainObject = jsonObject.getJSONObject("main");
                    JSONObject weatherObject = jsonObject.getJSONArray("weather").getJSONObject(0);

                    holder.textView.setText(Math.round(mainObject.getDouble("temp")) + " C " + jsonObject.getString("dt_txt"));

                    String imageUrl = "https://openweathermap.org/img/wn/" + weatherObject.getString("icon") + "@2x.png";
                    this.startLoadImage(imageUrl, holder);


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }


        }

    }

    public void startLoadImage(String url, final MyViewHolder holder) {


        ImageRequest imageRequest = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {

                        try {
                            holder.imageView.setImageBitmap(bitmap);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        //imageView.setImageResource(R.drawable.image_load_error);
                    }
                });

        holder.imagesQueue.add(imageRequest);
    }

    public void stopLoadImage(MyViewHolder holder) {


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {

        if (mDataset != null) {

            JSONArray forecast = mDataset.optJSONArray("list");
            if (forecast != null) {

                final int count = forecast.length();
                return count;
            }
        }
        return 0;
    }
}
