package com.example.sampleproj.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import com.android.volley.*;
import com.android.volley.toolbox.*;
import com.example.sampleproj.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A placeholder fragment containing a simple view.
 */
public class NewsFragment extends PlaceholderFragment {

    private RequestQueue imagesQueue;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_news, container, false);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        RequestNewsFeed();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.imagesQueue.stop();//cancel images loading
    }

    public void RequestNewsFeed() {
        final TextView textView2 = this.getView().findViewById(R.id.section_label2);

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = "https://newsapi.org/v2/top-headlines?sources=google-news&apiKey=" + getString(R.string.NewsAPI_apiKey);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        final int articlesDisplayed = RefreshList(response);

                        textView2.setText("Displayed " + articlesDisplayed + " articles.");

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        textView2.setText("That didn't work!" + " " + error.getMessage());

                    }
                });
        // Add the request to the RequestQueue.
        queue.add(jsonObjectRequest);
    }

    public int RefreshList(JSONObject json) {
        //Reset view and download queue
        LinearLayout listView = this.getView().findViewById(R.id.scrollViewLayout);
        listView.removeAllViewsInLayout();
        this.imagesQueue = Volley.newRequestQueue(getContext());

        if (json == null || !json.has("articles")) {
            return 0;
        }

        //Parse articles
        JSONArray articles;
        try {
            articles = json.getJSONArray("articles");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }

        final int maxCount = 25;
        final int count = Math.min(articles.length(), maxCount);


        for (int i = 0; i < count; i++) {
            try {
                JSONObject jsonObject = articles.getJSONObject(i);

                View newItem = LayoutInflater.from(this.getContext()).inflate(R.layout.news_list_template, null);

                final TextView textView = newItem.findViewById(R.id.textViewNewsTemplate);
                final ImageView imageView = newItem.findViewById(R.id.imageViewNewsTemplate);

                textView.setText(jsonObject.getString("title"));
                listView.addView(newItem);

                //Request image
                String imageUrl = jsonObject.getString("urlToImage");
                ImageRequest imageRequest = new ImageRequest(imageUrl,
                        new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap bitmap) {

                                try {
                                    imageView.setImageBitmap(bitmap);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }

                            }
                        }, 0, 0, null,
                        new Response.ErrorListener() {
                            public void onErrorResponse(VolleyError error) {
                                //imageView.setImageResource(R.drawable.image_load_error);
                            }
                        });

                this.imagesQueue.add(imageRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return count;
    }
}